package net.eneiluj.nextcloud.phonetrack.service;

import static net.eneiluj.nextcloud.phonetrack.service.LoggerService.BROADCAST_LOCATION_UPDATED;
import static net.eneiluj.nextcloud.phonetrack.service.SmsListener.BROADCAST_LOGJOB_LIST_UPDATED;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import net.eneiluj.nextcloud.phonetrack.R;
import net.eneiluj.nextcloud.phonetrack.android.activity.LogjobsListViewActivity;
import net.eneiluj.nextcloud.phonetrack.android.activity.SettingsActivity;
import net.eneiluj.nextcloud.phonetrack.model.DBLogjob;
import net.eneiluj.nextcloud.phonetrack.model.DBSession;
import net.eneiluj.nextcloud.phonetrack.persistence.PhoneTrackSQLiteOpenHelper;
import net.eneiluj.nextcloud.phonetrack.service.LoggerService;

import java.util.List;

import androidx.preference.PreferenceManager;

import org.jetbrains.annotations.Nullable;

public class ExternalCommandReceiver extends BroadcastReceiver {
    private static final String START_LOGGER = "start";
    private static final String STOP_LOGGER = "stop";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean listenToCommand = prefs.getBoolean(context.getString(R.string.pref_key_command), false);

        // Do nothing if the setting checkbox isn't checked
        if (!listenToCommand) {
            return;
        }

        if (intent != null) {
            String command = intent.getStringExtra("command");
            String logJob = intent.getStringExtra("logjob");

            if (command != null) {
                switch (command) {
                    case START_LOGGER:
                        startOrStopLogjobs(context, true, logJob);
                        break;
                    case STOP_LOGGER:
                        startOrStopLogjobs(context, false, logJob);
                        break;
                }
            }
        }
    }

    private void startOrStopLogjobs(Context context, boolean start, @Nullable String logjobName) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean resetOnToggle = prefs.getBoolean(context.getString(R.string.pref_key_reset_stats), false);
        PhoneTrackSQLiteOpenHelper db = PhoneTrackSQLiteOpenHelper.getInstance(context);
        List<DBLogjob> logjobs = db.getLogjobs();

        for (DBLogjob lj: logjobs) {
            if (logjobName == null|| logjobName.equals(lj.getTitle())) {
                // we toggle disabled logjobs if this is the start command
                // we toggle enabled logjobs if this is NOT the start command
                if ((start && !lj.isEnabled()) ||
                        (!start && lj.isEnabled())
                ) {
                    db.toggleEnabled(lj, null, resetOnToggle);

                    // let LoggerService know
                    Intent intent = new Intent(context, LoggerService.class);
                    intent.putExtra(LogjobsListViewActivity.UPDATED_LOGJOBS, true);
                    intent.putExtra(LogjobsListViewActivity.UPDATED_LOGJOB_ID, lj.getId());
                    context.startService(intent);

                    // update potential logjob list view
                    Intent broadcastIntent = new Intent(BROADCAST_LOCATION_UPDATED);
                    broadcastIntent.putExtra(LoggerService.BROADCAST_EXTRA_PARAM, lj.getId());
                    context.sendBroadcast(broadcastIntent);
                }
            }
        }
    }
}